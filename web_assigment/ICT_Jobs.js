var tableContent = [
    {
        role: "Software Developers & Programmers",
        numJobs: 590,
        SalaryMax: 100,
        SalaryMin: 72,
        Skills: ["computer software and systems", "programming languages and techniques", "software development processes such as Agile", "confidentiality", "data security and data protection issues"],
        JobDescription: "Software developers and programmers develop and maintain computer software, websites and software applications (apps).",
        imageName: "Job_Programmer"

    },

    {
        role: "Database & Systems Administrators",
        numJobs: 74,
        SalaryMax: 90,
        SalaryMin: 66,
        Skills: ["a range of database technologies and operating systems", "new developments in databases and security systems", "computer and database principles and protocols"],
        JobDescription: "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures.",
        imageName: "Job_DBAdmin"
    },
    {
        role: "Help Desk & IT Support",
        numJobs: 143,
        SalaryMax: 65,
        SalaryMin: 46,
        Skills: ["computer hardware, software, networks and websites", "the latest developments in information technology"],
        JobDescription: "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims.",
        imageName: "Job_helpDesk"
    },
    {
        role: "Data Analyst",
        numJobs: 270,
        SalaryMax: 128,
        SalaryMin: 69,
        Skills: ["data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R", " data analysis, mapping and modelling techniques", "analytical techniques such as data mining"],
        JobDescription: "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        imageName: "Job_DataAnalyst"
    },
    {
        role: "Test Analyst",
        numJobs: 127,
        SalaryMax: 98,
        SalaryMin: 70,
        Skills: ["programming methods and technology", "computer software and systems", " project management"],
        JobDescription: "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems.",
        imageName: "Job_TestAnalyst"
    },
    {
        role: "Project Management",
        numJobs: 188,
        SalaryMax: 190,
        SalaryMin: 110,
        Skills: ["principles of project management", "approaches and techniques such as Kanban and continuous testing", "how to handle software development issues", " common web technologies used by the scrum team"],
        JobDescription: "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress.",
        imageName: "Job_ProjectManager"
    }

];


function makeTable() {
    var tableDiv = document.getElementById("tableContainer");
    table = document.createElement("table");
    table.style.border = "solid";
    table.id = "theTable";

    var tbody = document.createElement("tbody");
    tbody.id = "theTableBody";


    var cell1 = document.createElement("th");
    cell1.innerHTML = "Role";
    var cell2 = document.createElement("th");
    cell2.innerHTML = "Job Vacancies 08/18";
    var cell3 = document.createElement("th");
    cell3.innerHTML = "Salary Max";
    var cell4 = document.createElement("th");
    cell4.innerHTML = "Salary Min";


    var row = document.createElement("tr");
    row.appendChild(cell1);
    row.appendChild(cell2);
    row.appendChild(cell3);
    row.appendChild(cell4);


    // Upper header
    var upperRow = document.createElement("tr");
    var upperCell1 = document.createElement("td");
    upperCell1.colSpan = 4;
    upperCell1.innerHTML = "Jobs in ICT - click to view more details";
    upperRow.appendChild(upperCell1);


    var thead = document.createElement("thead");
    thead.appendChild(upperRow);
    thead.appendChild(row);


    table.appendChild(thead);
    table.appendChild(tbody);
    tableDiv.appendChild(table);

    appendRows();
    appendFooter();

}

function appendRows() {

    for (var i = 0; i < tableContent.length; i++) {
        var row1 = document.createElement("tr");
        row1.className = "clickable_rows";

        var att = document.createAttribute("onclick");
        att.value = "chooseJob(" + i + ")";
        row1.setAttributeNode(att);


        var cell1_1 = document.createElement("td");
        cell1_1.innerHTML = tableContent[i].role;

        var cell1_2 = document.createElement("td");
        cell1_2.innerHTML = tableContent[i].numJobs;

        var cell1_3 = document.createElement("td");
        cell1_3.innerHTML = tableContent[i].SalaryMax;

        var cell1_4 = document.createElement("td");
        cell1_4.innerHTML = tableContent[i].SalaryMin;


        row1.appendChild(cell1_1);
        row1.appendChild(cell1_2);
        row1.appendChild(cell1_3);
        row1.appendChild(cell1_4);
        table.appendChild(row1);
    }
}

function appendFooter() {
    var cellSummary1 = document.createElement("td");
    cellSummary1.innerHTML = "";
    var cellSummary2 = document.createElement("td");
    cellSummary2.innerHTML = "Total: " + totalNumJobs();
    var cellSummary3 = document.createElement("td");
    cellSummary3.innerHTML = "Average: " + averageMaxSalary();
    var cellSummary4 = document.createElement("td");
    cellSummary4.innerHTML = "Average: " + averageMinSalary();

    var summaryRow = document.createElement("tfoot");
    summaryRow.appendChild(cellSummary1);
    summaryRow.appendChild(cellSummary2);
    summaryRow.appendChild(cellSummary3);
    summaryRow.appendChild(cellSummary4);
    table.appendChild(summaryRow);
}

function totalNumJobs() {
    var total = 0;
    for (var i = 0; i < tableContent.length; i++) {
        total += tableContent[i].numJobs;
    }
    return total;
}

function averageMaxSalary() {
    var avg = 0;
    var sum = 0;
    for (var i = 0; i < tableContent.length; i++) {
        sum += tableContent[i].SalaryMax;
    }
    avg = Math.floor(sum / tableContent.length);
    return avg;
}

function averageMinSalary() {
    var avg = 0;
    var sum = 0;
    for (var i = 0; i < tableContent.length; i++) {
        sum += tableContent[i].SalaryMin;
    }
    avg = Math.floor(sum / tableContent.length);
    return avg;
}


function changeColorIfSelected(item) {
    var clickableRow = document.getElementsByClassName("clickable_rows");
    for (var i = 0; i < clickableRow.length; i++) {
        clickableRow[i].style.background = "LightGrey";
        if (i != item) {
            clickableRow[i].style.background = "white";
        }
    }
}

function makeList(ind) {
    var array = tableContent[ind].Skills;
    var ul = document.createElement("ul");
    ul.id = "list";

    for (var i = 0; i < array.length; i++) {
        var li = document.createElement("li");
        li.className = "listItems";
        li.innerHTML = array[i];
        ul.appendChild(li);
    }
    document.getElementById("listOfSkills").appendChild(ul);
}

function clearList() {
    document.getElementById("listOfSkills").innerHTML = "";
}


function changeImage(numImage) {
    var jobImage = document.getElementById("changingPicture");
    jobImage.src = "images/" + tableContent[numImage].imageName + ".jpg";
    jobImage.alt = tableContent[numImage].role;
}

function changeJobTitle(i) {
    var jobTitle = document.getElementById("job_title");
    jobTitle.innerText = tableContent[i].role;

}

function changeDescription(i) {
    var jobDescription = document.getElementById("job_description");
    jobDescription.innerText = tableContent[i].JobDescription;
}


function chooseJob(i) {
    changeColorIfSelected(i);
    changeImage(i);
    changeJobTitle(i);
    changeDescription(i);
    clearList();
    makeList(i);
}

